### Assignment 4.2 ###

	This project completes the assignment 4.2 in the textbook. It estimates the value of pi using a Monte Carlo method, in this case by simulating darts being thrown at a dartboard and dividing the number that land in the circle by the total number of tosses. It splits up this task into multiple threads in order to perform the calculations more quickly.

To compile and run from command line:
```
g++ pThreads_Timing.cpp CStopWatch.cpp
./a.out
```
or
```
   cd Default && make all
```

To compile and run with Docker:
```
docker run --rm -v c:/Users/Michael/Desktop/CS4170_FA2018_R03_RYGULA:/tmp -w /tmp/Default rgreen13/alpine-bash-gpp make all
docker run --rm -v c:/Users/Michael/Desktop/CS4170_FA2018_R03_RYGULA:/tmp -w /tmp/Default rgreen13/alpine-bash-gpp ./pThreads_Timing
```


### REFLECTION ###

a) What was the major question or topic addressed in the classroom?
	The main topics covered this week were "How do we use Docker and GitLab for this class?" and "How can we measure and implement optimization of pThreads used to perform calulations?"

b) Summarize what you learned during this week of class. Note any particular features, concepts, code,
or terms that you believe are important.

	� References and examples of your completed work must be included! If you include a screenshot,
	code, formula, etc., you must also explain it clearly.

	� Note that naming a concept does not demonstrate learning. If you name a concept or topic,
	spend at least 1-2 sentences explaining what it is or how it works.
	
	This week we learned how to create and use pThreads properly. Returning variables from pThread functions is not as simple as I previously thought last week. I will explain the process with some examples. Before we begin, we must include the library at the top of our program through "#include <pthread.h>". Now we must declare a pointer to act as a handler for the pThreads, we use this to identify which thread we are in: "pthread_t* thread_handles;" Next, we declare an array like so: "thread_handles = new pthread_t[threadMax];" This is used to keep track of the threads in an array. We then have to actually create the threads using "pthread_create(&thread_handles[thread], NULL, getSamples, (void*) (numSamples/numThreads));" The first parameter relates to the new thread's position in the array, the second parameter allows us to determine attributes for the new thread but NULL sets these to default values, the third parameter allows our new thread to run the "getSamples" function that estimates pi, and the final parameter allows us to distribute the amount of samples evenly between threads. Now we use "pthread_join(thread_handles[thread], (void **)&retValue);" to peek inside of each thread and add together the values of their individual estimates so that we can determine our final estimate. The first parameter is the ID of the thread and the second parameter contains a pointer to the returned solution to that thread's "getSamples" function. Finally, We divide this total by the number of threads to get our real estimate. An important thing to note is that we must delete the handler for the threads once we are done using them in order to prevent a memeory leak, which are to be avoided whenever possible. We do this simply though "delete thread_handles;"

c) What was the most difficult aspect of what you learned in class & the lab experience? Why was this
the most difficult aspect? What can you do to make it easier for you? Particularly focus on what you
accomplished in class and what kept you from completing the assignment in class.

	One of the most difficult parts for me was simply getting Docker and GitLab to work properly on my computer. Other than that aspect, one of the things that is tricky for me is wrapping my head around the concepts of how threads are coded. I really dislike pointers, and a lot of using pthreads entails heavy utilization of pointers and knowledge of how memory locations are stored and accessed. I could improve myself in this area by perhaps brushing up on my operating system concepts. 

d) Analyze the results of your code, commenting on speedup, efficiency, and scalability

	I was not able to finish my program before the solution was released, so its not complete. However, I can comment on the program as a whole. I believe that this program is scalable because the efficiency stays level as the number of threads goes up because there is not much linear code. The speedup is high, because of the amount and type of calculations is greatly affected by the number of threads in use.