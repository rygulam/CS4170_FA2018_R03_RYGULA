// USE THIS TO GENERATE RANDOM NUMBERS
// x = ((double) std::rand() / (double) RAND_MAX) * 2.0 - 1.0;
// y = ((double) std::rand() / (double) RAND_MAX) * 2.0 - 1.0;
// try and solve this by yourself, save stack overflow for after next class meeting

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h> 
#include <iostream>
#include <unistd.h>

#include "CStopWatch.h"

/*-------------------------------------------------------------------*/
void *getSamples(void* nSamples) {
   
   //long numSamples = (long)nSamples;
	long* numSamples = new long();

	*numSamples = (long)nSamples;

   for(int i=0; i< numSamples; i++){
      usleep(std::rand() % 5000 + 1);
   }
   // THIS METHOD WILL NOT WORK DUE TO HOW SCOPES WORK IN C++
   // EVERY THREAD GETS ITS OWN STACK/HEAP
   // THIS WILL CAUSE SEG FAULTS BECAUSE THE MEMORY WILL BE DE-ALLOCATED WHEN YOU LEAVE THIS LOCALLY
   // return (void*)&numSamples;
   // DO THIS FORM OF numSamples INSTEAD
   return NULL;
}  

int main() {
   long        threadMin, threadMax, threadStep;
   long        sampleMin, sampleMax, sampleStep;
   long        numTrials;
   pthread_t*  thread_handles; 
   CStopWatch  timer;
   void*	result;

   threadMin  = 10; 
   threadMax  = 100;
   threadStep = 10;

   sampleMin  = 1000;
   sampleMax  = 10000;
   sampleStep = 1000;

   numTrials  = 10;
   
   thread_handles = new pthread_t[threadMax]; 

   
   for(int numThreads=threadMin; numThreads<threadMax; numThreads+=threadStep){

      for(long numSamples=sampleMin; numSamples < sampleMax; numSamples+=sampleStep){
         for(int curTrial=0; curTrial<numTrials; curTrial++){
            timer.startTimer();
            for (long thread = 0; thread < numThreads; thread++){
               pthread_create(&thread_handles[thread], NULL, getSamples, (void*) (numSamples/numThreads));  
            }

            for (int thread = 0; thread < numThreads; thread++){
               pthread_join(thread_handles[thread], &result);

			   //dynamic memory that must be deallocated
			   myEstimate += *(double*)result;
			   delete (double*)result;
            }
            timer.stopTimer();

            std::cout << numThreads << " Threads : " << numSamples << " samples " << timer.getElapsedTime() << " seconds\n";;

         }
      }
   }

   delete thread_handles;

   return 0;
} 